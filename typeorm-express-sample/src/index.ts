import "reflect-metadata";
import {createConnection} from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import {Request, Response} from "express";
import {Routes} from "./routes";
import {User} from "./entity/User";

createConnection().then(async connection => {

    // create express app
    const app = express();
    app.use(bodyParser.json());

    var router = express.Router();

    // a global logging middleware
    router.use("/", (req: Request, res: Response, next: Function) => {
      console.log(`[${req.method}]: ${req.originalUrl}`);
      next();
    });

    // register express routes from defined application routes
    Routes.forEach((route) => {
      //if present, register middlewares in the order they were given
      if (route.middlewares) {
        route.middlewares.forEach((middleware) => {
          (router as any)[route.method](route.route, middleware);
        });
      }
      (router as any)[route.method](
        route.route,
        (req: Request, res: Response, next: Function) => {
          const result = new (route.controller as any)()[route.action](
            req,
            res,
            next
          );
          if (result instanceof Promise) {
            result.then((result) =>
              result !== null && result !== undefined
                ? res.send(result)
                : undefined
            );
          } else if (result !== null && result !== undefined) {
            res.json(result);
          }
        }
      );

    });

    app.use("/", router);

    const result = require("dotenv").config();

    if (result.error) {
      throw result.error;
    }

    // setup express app here
    // ...

    // start express server
    app.listen(3000);

    // insert new users for test
   /* await connection.manager.save(connection.manager.create(User, {
        firstName: "Timber",
        lastName: "Saw",
        age: 27
    }));
    await connection.manager.save(connection.manager.create(User, {
        firstName: "Phantom",
        lastName: "Assassin",
        age: 24
    }));
*/
    console.log("Express server has started on port 3000. Open http://localhost:3000/users to see results");

}).catch(error => console.log(error));
