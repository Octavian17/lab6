import { UserController } from "./controller/UserController";
import { needsAuth } from "./middlewares/AuthenticationMiddleware";
import {
  checkExistingUserId,
  checkValidUserBodySave,
  checkValidUserBodyLogin,
} from "./middlewares/ValidationMiddleware";

export const Routes = [
  {
    method: "get",
    route: "/users",
    controller: UserController,
    action: "all",
    middlewares: [needsAuth],
  },
  {
    method: "get",
    route: "/users/:id",
    controller: UserController,
    action: "one",
    middlewares: [needsAuth, checkExistingUserId],
  },
  {
    method: "put",
    route: "/users/:id",
    controller: UserController,
    action: "update",
    middlewares: [needsAuth, checkExistingUserId],
  },
  {
    method: "post",
    route: "/users",
    controller: UserController,
    action: "save",
    middlewares: [needsAuth, checkValidUserBodySave],
  },
  {
    method: "delete",
    route: "/users/:id",
    controller: UserController,
    action: "remove",
    middlewares: [needsAuth, checkExistingUserId],
  },
  {
    method: "post",
    route: "/register",
    controller: UserController,
    action: "register",
    middlewares: [checkValidUserBodySave],
  },
  {
    method: "get",
    route: "/login",
    controller: UserController,
    action: "login",
    middlewares: [checkValidUserBodyLogin],
  },
];
