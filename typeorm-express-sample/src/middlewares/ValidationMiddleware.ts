import { NextFunction, Request, Response } from "express";
import { getRepository } from "typeorm";
import { User } from "../entity/User";

export async function checkExistingUserId(
  request: Request,
  response: Response,
  next: NextFunction
) {
  let userRepository = getRepository(User);

  if (Number.isNaN(Number(request.params.id))) {
    response.status(400).send("Please provide a valid user id");
    return;
  }
  let userToRemove = await userRepository.findOne(request.params.id);
  if (!userToRemove) {
    response
      .status(404)
      .send(`User with id ${request.params.id} does not exist`);
    return;
  }

  return next();
}

function checkValidUserBody(
  request: Request,
  response: Response,
  next: NextFunction,
  checkUserName: boolean = false
) {
  if (
    (checkUserName && !request.body.userName) ||
    !request.body.email ||
    !request.body.password
  ) {
    response.status(400).send("Please provide all the necessary fields");
    return;
  }

  return next();
}

export function checkValidUserBodySave(
  request: Request,
  response: Response,
  next: NextFunction
) {
  return checkValidUserBody(request, response, next, true);
}

export function checkValidUserBodyLogin(
  request: Request,
  response: Response,
  next: NextFunction
) {
  return checkValidUserBody(request, response, next);
}
