const Datastore = require('@google-cloud/datastore');
const datastore = new Datastore({
	projectId: 'fsdproject-338315',
	keyFilename: 'datastore-credential.json'
});
const kindName = 'user-log';

// exports.savelog = (req, res) => {
// 	let start_date  = req.query.start_date || req.body.start_date || 0;
// 	let end_date  = req.query.end_date || req.body.end_date || 0;
// 	let log = req.query.log || req.body.log || '';

// 	datastore
// 		.save({
// 			key: datastore.key(kindName),
// 			data: {
// 				log: log,
// 				start_date: datastore.datetime(start_date),
// 				end_date: datastore.datetime(end_date)
// 			}
// 		})
// 		.catch(err => {
// 		    console.error('ERROR:', err);
// 		    res.status(200).send(err);
// 		    return;
// 		});

// 	res.status(200).send(log);
// };



// https://us-central1-fsdproject-338315.cloudfunctions.net/saveDates

exports.saveDates = async (req, res) => {
  res.set({
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods": "POST",
    "Access-Control-Allow-Headers": "*",
  });

  const start_date = req.body.start_date;
  const end_date = req.body.end_date;

 
    datastore
      .save({
        key: datastore.key(kindName),
        data: {
          start_date: datastore.string(start_date),
          end_date: datastore.string(end_date),
		  
        },
      })
      .catch((err) => {
        console.error("ERROR:", err);
        res.status(500).send(err);
        return;
      });

    res.status(200).send("Added succesfully");
  

  res.status(500).send("ERROR!");
  return;
};



// https://us-central1-fsdproject-338315.cloudfunctions.net/getDates

exports.getDates = async (req, res) => {
  res.set({
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods": "GET",
    "Access-Control-Allow-Headers": "*",
  });

  const query = datastore.createQuery(kindName);

  datastore
    .runQuery(query)
    .then((dates) => {
      res.status(200).send(dates[0]);
    })
    .catch((err) => {
      console.error("ERROR:", err);
      res.status(500).send(err);
      return;
    });
};


//gcloud functions deploy saveDates --entry-point saveDates --runtime nodejs16 --trigger-http --allow-unauthenticated --project fsdscheduler
//gcloud functions deploy getDates --entry-point getDates --runtime nodejs16 --trigger-http --allow-unauthenticated --project fsdscheduler
//https://console.cloud.google.com/datastore/entities;kind=user-log;ns=__$DEFAULT$__/query/kind?project=fsdproject-338315